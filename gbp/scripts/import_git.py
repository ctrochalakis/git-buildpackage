# vim: set fileencoding=utf-8 :

from gbp.scripts.import_orig import *

def build_parser(name):
    try:
        parser = GbpOptionParserDebian(command=os.path.basename(name), prefix='',
                                       usage='%prog [options] ')
    except GbpError as err:
        gbp.log.err(err)
        return None

    merge_group = GbpOptionGroup(parser, "options", "options related to git import")
    parser.add_option_group(merge_group)

    merge_group.add_config_file_option(option_name="debian-branch",
                                       dest="debian_branch")
    merge_group.add_config_file_option(option_name="upstream-tag",
                                       dest="upstream_tag")
    merge_group.add_option("-u", "--upstream-version", dest="version",
                           help="Upstream Version")

    parser.add_boolean_config_file_option(option_name="rollback",
                                                      dest="rollback")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
                      help="verbose command execution")
    parser.add_config_file_option(option_name="color", dest="color", type='tristate')
    parser.add_config_file_option(option_name="color-scheme",
                                  dest="color_scheme")

    return parser


def parse_args(argv):
    """Parse the command line arguments
    @return: options and arguments
    """

    parser = build_parser(argv[0])
    if not parser:
        return None, None

    (options, args) = parser.parse_args(argv[1:])
    gbp.log.setup(options.color, options.verbose, options.color_scheme)

    if not options.version:
        raise GbpError("you must pass a version to import.")


    return options, args


def main(argv):
    ret = 0
    repo = None

    (options, args) = parse_args(argv)
    if not options:
        return ExitCodes.parse_error

    try:
        try:
            repo = ImportOrigDebianGitRepository('.')
        except GitRepositoryError:
            raise GbpError("%s is not a git repository" % (os.path.abspath('.')))

        is_empty = repo.is_empty()

        (clean, out) = repo.is_clean()
        if not clean and not is_empty:
            gbp.log.err("Repository has uncommitted changes, commit these first: ")
            raise GbpError(out)

        tag = repo.version_to_tag(options.upstream_tag, options.version)
        gbp.log.info('Using tag %s' % tag)

        repo.rrr_branch(options.debian_branch)
        debian_branch_merge_by_replace(repo, tag, options.version, options)
    except GbpError as err:
        if str(err):
            gbp.log.err(err)
        ret = 1
        if repo and repo.has_rollbacks() and options.rollback:
            gbp.log.err("Error detected, Will roll back changes.")
            try:
                repo.rollback()
                # Make sure the very last line as an error message
                gbp.log.err("Rolled back changes after import error.")
            except Exception as e:
                gbp.log.err("%s" % e)
                gbp.log.err("Clean up manually and please report a bug: %s" %
                            repo.rollback_errors)

    if not ret:
        gbp.log.info("Successfully updated version %s" % options.version)
    return ret


if __name__ == "__main__":
    sys.exit(main(sys.argv))

# vim:et:ts=4:sw=4:et:sts=4:ai:set list listchars=tab\:»·,trail\:·:
